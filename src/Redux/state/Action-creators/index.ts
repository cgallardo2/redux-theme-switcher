import { ActionType } from "../Action-types";

import { Dispatch } from "redux";
import { Action } from "../Actions";

export const darkMode = (theme: string, title: string) => {
  return (dispatch: Dispatch<Action>) => {
    dispatch({
      type: ActionType.DARKMODE,
      payload: "dark mode",
      title: title,
    });
  };
};

export const lightMode = (theme: string, title: string) => {
  return (dispatch: Dispatch) => {
    dispatch({
      type: ActionType.LIGHTMODE,
      payload: "light mode",
      title: title,
    });
  };
};
