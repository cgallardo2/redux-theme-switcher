import { ActionType } from "../Action-types/index";
interface DarkModeAction {
  type: ActionType.DARKMODE;
  payload: string;
  title: string;
}
interface LightModeAction {
  type: ActionType.LIGHTMODE;
  payload: string;
  title: string;
}

export type Action = DarkModeAction | LightModeAction;
