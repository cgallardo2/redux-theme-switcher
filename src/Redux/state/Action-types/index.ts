export enum ActionType {
  DARKMODE = "dark",
  LIGHTMODE = "light",
}
