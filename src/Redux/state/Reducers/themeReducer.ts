import { ActionType } from "../Action-types";
import { Action } from "../Actions";

const initialState = "light";

//  interface Action{
//     type:string,
//     payload?:number,
//  }

const reducer = (state: string = initialState, action: Action) => {
  switch (action.type) {
    case ActionType.DARKMODE:
      return "dark";
    case ActionType.LIGHTMODE:
      return "light";

    default:
      return state;
  }
};

export default reducer;
