import { combineReducers } from "redux";
import themeReducer from "./themeReducer";

const reducers = combineReducers({
  themes: themeReducer,
});

export default reducers;

export type State = ReturnType<typeof reducers>;
