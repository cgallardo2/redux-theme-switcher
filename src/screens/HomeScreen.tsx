/** @jsxImportSource @emotion/react */
import React, { useState, useEffect, useRef } from "react";
import { bindActionCreators } from "redux";
import { Theme, css } from "@emotion/react";
import { useSelector, useDispatch } from "react-redux";
import { lightTheme, darkTheme } from "../Theme";
import { actionCreators, State } from "../Redux/state";
import { ActionType } from "../Redux/state/Action-types";
import { Action } from "../Redux/state/Actions";

export default function HomeScreen() {
  const myContainer = useRef<HTMLDivElement>(null);

  const dispatch = useDispatch();
  const { darkMode, lightMode } = bindActionCreators(actionCreators, dispatch);
  const Theme = useSelector((state: State) => state.themes);
  console.log("this is the theme: " + Theme);

  return (
    <>
      {Theme === ActionType.LIGHTMODE ? (
        <div css={Container("#E8AFA8", "black")}>
          <div css={TextContainer("black")}>
            <h1 css={titleStyle}>Light Theme!</h1>
          </div>
          <button
            css={Button("#D8776B", "#97534A", "white")}
            onClick={() => darkMode(ActionType.DARKMODE, "Dark Theme!")}
          >
            Change to dark mode
          </button>
        </div>
      ) : (
        <div css={Container("#000000", "white")}>
          <div css={TextContainer("white")}>
            <h1 css={titleStyle}>Dark Theme!</h1>
          </div>
          <button
            css={Button("#a01bf5", "#6c0da8", "white")}
            onClick={() => lightMode(ActionType.LIGHTMODE, "Light Theme!")}
          >
            Change to light mode
          </button>
        </div>
      )}

      {/* <div ref={myContainer} css={container}>
        <div css={TextContainer}>
          <h1 css={titleStyle}>Hello world!</h1>
        </div>
        {Theme === ActionType.LIGHTMODE ? (
          <button css={Button} onClick={() => darkMode(ActionType.DARKMODE)}>
            Change to dark mode
          </button>
        ) : (
          <button css={Button} onClick={() => lightMode(ActionType.LIGHTMODE)}>
            Change to light mode
          </button>
        )}
      </div> */}
    </>
  );
}

const Container = (bg_color: string, text_color: string) => css`
  background-color: ${bg_color};
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  color: ${text_color};
`;
const TextContainer = (border: string) => css`
  border: 3px solid ${border};
  padding: 16px;
  border-radius: 6px;
`;

const Button = (
  bg_color: string,
  bg_color_hover: string,
  text_color: string
) => css`
  margin: 32px;
  background-color: ${bg_color};
  padding: 16px 32px;
  border-radius: 6px;
  border: none;

  font-size: 15px;
  font-weight: 500px;
  color: ${text_color};

  &:hover {
    background-color: ${bg_color_hover};
  }
`;

const titleStyle = (theme: Theme) => css`
  font-size: 40px;
`;
//other way to do it
const divStyle = css({
  flex: 1,
  backgroundColor: "#E8AFA8",
  alignItems: "center",
  justifyContent: "center",
  color: "red",
});

// const divStyle2 = css({
//   backgroundColor: ${(props) => props.theme.PRIMARY_BACKGROUND_COLOR},
//   position: "absolute",
//   top: 0,
//   bottom: 0,
//   left: 0,
//   right: 0,
//   display: "flex",
//   flexWrap: "wrap",
//   flexDirection: "column",
//   alignItems: "center",
//   justifyContent: "center",
// });
