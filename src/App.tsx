/** @jsxImportSource @emotion/react */
import React from "react";
import HomeScreen from "./screens/HomeScreen";
import { Theme, css, jsx } from "@emotion/react";
import { Provider } from "react-redux";
import { createStore, applyMiddleware, combineReducers } from "redux";
import thunk from "redux-thunk";
import reducer from "./Redux/state/Reducers/themeReducer";
import { store } from "./Redux/state";
function App() {
  return (
    <Provider store={store}>
      <HomeScreen />
    </Provider>
  );
}

export default App;
